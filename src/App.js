import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './components/header/Header';
import SearchBar from './components/search-bar/SearchBar';
import TodoList from './components/todo-list/TodoList';
import Footer from './components/footer/Footer';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            keyword: '',
            items: this.getItems()
        };
    }

    getItems() {
        return [
            {
                id: 1,
                name: 'Todo item number one'
            },
            {
                id: 2,
                name: 'Todo item number two'
            },
            {
                id: 3,
                name: 'Todo item number three'
            },
            {
                id: 4,
                name: 'Todo item number for'
            },
            {
                id: 5,
                name: 'Todo item number five'
            },
        ];
    }

    onSearch = (keyword) => {
        this.setState(prevState => {
            return {
                keyword: keyword
            }
        });

        this.setState(prevState => {
            if (keyword.length > 2) {
                return {
                    items: this.state.items.filter( item => item.name.indexOf(keyword) > 0 )
                }
            }

            return {
                items: this.getItems()
            }
        });
    };

    render() {
        return (
            <div className="container">
                {/* Header Component*/}
                <Header/>

                {/* Search Component */}
                <SearchBar search={this.onSearch}/>

                {/* TodoList Component */}
                <TodoList keyword={this.state.keyword} items={this.state.items}/>

                {/* Footer Component */}
                <Footer/>
            </div>
        );
    }
}

export default App;
