import React, { Component } from "react";
import './SearchBar.css';

export default class SearchBar extends Component {
    constructor(props) {
        super(props);

        console.log(props.items)
    }

    render () {
        return (
            <div id="search-bar" className="row">
                <div className="col-sm-11 form-group">
                    <input type="text" onKeyUp={(e) => this.props.search(e.currentTarget.value)} className="form-control" placeholder="Search" />
                </div>
                <div className="col-sm-1 pull-left search">
                    <button className="btn btn-primary">Search</button>
                </div>
            </div>
        );
    }
}