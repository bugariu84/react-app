import React, { Component } from 'react';

class TodoItem extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <li key={this.props.item.id}>{this.props.item.name}</li>
        );
    };
}

export default TodoItem;