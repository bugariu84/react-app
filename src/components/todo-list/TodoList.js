import React, { Component } from 'react';
import TodoItem from '../TodoItem/TodoItem';

export default class TODOList extends Component {
    constructor(props) {
        super(props);

    }

    render () {
        let todoItems = this.props.items.map((item, index) => <TodoItem item={item} key={index}/>);

        return (
            <div>
                <p>Search keyword: {this.props.keyword}</p>
                <ol>{todoItems}</ol>
            </div>
        );
    }
}